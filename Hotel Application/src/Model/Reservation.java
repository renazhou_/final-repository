/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.Date;

import java.util.logging.Logger;
import javafx.scene.control.TableColumn;
import javafx.stage.Stage;

/**
 *
 * @author renazhou
 */
public class Reservation {
    int ID;
    String refCode;
    String guestID;
    String numPeople;
    String roomNumber;
    Date checkIn;
    Date checkOut;
    String adjustedCheck;
    String freeBreakfast;
    int depositPaid;
    int resCost;

    public Reservation(String refCode, String guestID, String numPeople, String roomNumber, Date checkIn, Date checkOut, String adjustedCheck, String freeBreakfast, int depositPaid, int resCost) {
        this.refCode = refCode;
        this.guestID = guestID;
        this.numPeople = numPeople;
        this.roomNumber = roomNumber;
        this.checkIn = checkIn;
        this.checkOut = checkOut;
        this.adjustedCheck = adjustedCheck;
        this.freeBreakfast = freeBreakfast;
        this.depositPaid = depositPaid;
        this.resCost = resCost;
    }

    public int getID() {
        return ID;
    }
    

    public String getRefCode() {
        return refCode;
    }

    public String getGuestID() {
        return guestID;
    }

    public String getNumPeople() {
        return numPeople;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public Date getCheckIn() {
        return checkIn;
    }

    public Date getCheckOut() {
        return checkOut;
    }

    public String getAdjustedCheck() {
        return adjustedCheck;
    }

    public void setAdjustedCheck(String adjustedCheck) {
        this.adjustedCheck = adjustedCheck;
    }
   

    public String getFreeBreakfast() {
        return freeBreakfast;
    }

    public int getDepositPaid() {
        return depositPaid;
    }
    
    public int getResCost(){
        return resCost;
    }

    
    

    public void setRefCode(String refCode) {
        this.refCode = refCode;
    }

    public void setName(String guestID) {
        this.guestID = guestID;
    }

    public void setNumPeople(String numPeople) {
        this.numPeople = numPeople;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public void setCheckIn(Date checkIn) {
        this.checkIn = checkIn;
    }

    public void setCheckOut(Date checkOut) {
        this.checkOut = checkOut;
    }

    public void setGuestID(String guestID) {
        this.guestID = guestID;
    }
    

    public void setFreeBreakfast(String freeBreakfast) {
        this.freeBreakfast = freeBreakfast;
    }

    public void setDepositPaid(int depositPaid) {
        this.depositPaid = depositPaid;
    }
    
    public void setResCost(int resCost){
        this.resCost = resCost;
    }
        
    

}
