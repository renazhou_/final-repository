/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Tranny
 */
public class Purchase {
    //String purchaseID;
    int purchaseQuantity;
    String purchaseDesc;
    Double purchaseCost;
   // String invoiceID;

    public Purchase(int purchaseQuantity, String purchaseDesc, Double purchaseCost) {
       // this.purchaseID = purchaseID;
        this.purchaseQuantity = purchaseQuantity;
        this.purchaseDesc = purchaseDesc;
        this.purchaseCost = purchaseCost;
       // this.invoiceID = invoiceID;
    }

    public int getPurchaseQuantity() {
        return purchaseQuantity;
    }



    public String getPurchaseDesc() {
        return purchaseDesc;
    }

    public Double getPurchaseCost() {
        return purchaseCost;
    }

    public void setPurchaseQuantity(int purchaseQuantity) {
        this.purchaseQuantity = purchaseQuantity;
    }



    public void setPurchaseDesc(String purchaseDesc) {
        this.purchaseDesc = purchaseDesc;
    }

    public void setPurchaseCost(Double purchaseCost) {
        this.purchaseCost = purchaseCost;
    }

    @Override
    public String toString() {
        return purchaseQuantity + "x " + purchaseDesc + " "+ purchaseCost + "each";
    }

    
    
}
