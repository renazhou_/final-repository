/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.application;

import Model.Purchase;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfWriter;
import hotel.application.database.DatabaseSetup;
import hotel.application.database.GuestQuery;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javafx.application.Application.launch;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import com.itextpdf.text.pdf.PdfPTable;
import hotel.application.database.ReservationQuery;
import javafx.scene.control.ComboBox;


/**
 * FXML Controller class
 *
 * @author Tranny
 */
public class PurchasesController implements Initializable {

    @FXML
    private ComboBox RefCodeComboBox;
    
    @FXML
    private TextArea itemDescriptionTextArea;
    
    @FXML
    private TextField itemCostTextField;
    
    @FXML
    private TextField quantityTextField;
    
    @FXML
    private ListView purchasesListView;
    
    @FXML
    private Button addPurchasesButton;
    
    @FXML
    private Button printButton;
    
    @FXML
    private Button cancelButton;
    
    private GuestQuery guestQuery = new GuestQuery();
    
    private ObservableList<Purchase> purchasesList;
    
    
    @FXML
    private void addPurchaseButton(ActionEvent event) {
        int newPurchaseQuantity = Integer.parseInt(quantityTextField.getText());
        String newPurchaseDesc = itemDescriptionTextArea.getText();
        Double newPurchaseCost = Double.parseDouble(itemCostTextField.getText());
        
        Purchase newPurchase = new Purchase(newPurchaseQuantity, newPurchaseDesc, newPurchaseCost);
        System.out.println("guestQuery = "+guestQuery);
        System.out.println("newPurchase = " +newPurchase);
        guestQuery.insertPurchase(newPurchase);
        purchasesList.add(newPurchase);
  
        
    }
    
    
            
         
       @FXML
    private void printButton(ActionEvent event) {
        try {
            //create document
            Document document = new Document();
            
            FileOutputStream output = null;


            output = new FileOutputStream("myPDF.pdf");
            
            //create PdfWriter
            PdfWriter.getInstance(document, output);
            
            document.open();
            //can now add content to document
            
            PdfPTable table = new PdfPTable(3);
            //TODO heading
            //When each thing happened (enter it using datepicker)
            //subttotals
            //use currently occupied room number instead of guestID
            PdfPCell cell1 = new PdfPCell(new Paragraph("Quantity"));
            PdfPCell cell2 = new PdfPCell(new Paragraph("Purchase Item"));
            PdfPCell cell3 = new PdfPCell(new Paragraph("Purchase Cost"));
            table.addCell(cell1);
            table.addCell(cell2);
            table.addCell(cell3);
            
            for(Purchase p : purchasesList) {
            PdfPCell cell4 = new PdfPCell(new Paragraph(p.getPurchaseQuantity()));
            PdfPCell cell5 = new PdfPCell(new Paragraph(p.getPurchaseDesc()));
            PdfPCell cell6 = new PdfPCell(new Paragraph(p.getPurchaseCost().toString())); 
            table.addCell(cell4);
            table.addCell(cell5);
            table.addCell(cell6);
            }
            
            //PdfPCell cell8 = new PdfPCell(new Paragraph("Subtotal"));
            //table.addCell(cell8);
            document.add(new Phrase("Thank you for staying at ____'s Hotel! " ));
            

            document.add(table); 
            

            
            document.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(PurchasesController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DocumentException ex) {
            Logger.getLogger(PurchasesController.class.getName()).log(Level.SEVERE, null, ex);
        }

  
        
    }
      
    @FXML
    private void handleCancelButton(ActionEvent event) throws IOException{
        Parent root = FXMLLoader.load(getClass().getResource("Dashboard.fxml"));
        Scene scene = new Scene(root);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(scene);
        app_stage.show();
            
    }
    
        
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        DatabaseSetup.setupDatabase();
        purchasesList = FXCollections.observableArrayList(guestQuery.getPurchases());
        purchasesListView.setItems(purchasesList);
        
                //comboBox values
        ObservableList<String> refCodeOptions = FXCollections.observableArrayList();

        ReservationQuery rq = new ReservationQuery();
        refCodeOptions.addAll(rq.getRefCodes());

        RefCodeComboBox.setItems(refCodeOptions);

    }    
    
}
