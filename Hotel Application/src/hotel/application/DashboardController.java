/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.application;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Tranny
 */
public class DashboardController implements Initializable {

    @FXML
    private Pane customersPaneButton;

    @FXML
    private Pane roomsPaneButton;

    @FXML
    private Pane purchasesPaneButton;

    @FXML
    private Pane staffPaneButton;
    
    @FXML
    private Pane reservationPaneButton;

    @FXML
    private void handleReservationButton(Event event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("ReservationView.fxml"));        
        Scene scene = new Scene(root);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(scene);
        app_stage.show();
    }


    @FXML
    private void handleRoomButton(Event event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("RoomView.fxml"));
        Scene scene = new Scene(root);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(scene);
        app_stage.show();
    }
    
    @FXML
    private void handleCustomerButton(Event event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("CustomerView.fxml"));
        Scene scene = new Scene(root);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(scene);
        app_stage.show();
    }

    @FXML
        private void handlePurchasesButton(Event event) throws IOException {         
        Parent root = FXMLLoader.load(getClass().getResource("Purchases.fxml"));
        Scene scene = new Scene(root);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(scene);
        app_stage.show();
    }

    @FXML
        private void handleStaffButton(Event event) throws IOException {
            //TO DO:   MAKE STAFF DISPLAY GUI and change
        Parent root = FXMLLoader.load(getClass().getResource("RoomView.fxml"));
        Scene scene = new Scene(root);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(scene);
        app_stage.show();
    }

    @FXML
        private void handleLogOutButton(Event event) throws IOException{
        Parent root = FXMLLoader.load(getClass().getResource("LoginPage.fxml"));
        Scene scene = new Scene(root);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(scene);
        app_stage.show();
    
    }  
            
    @Override
        public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }   
    
}
