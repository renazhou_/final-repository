/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.application.database;

import Model.Staff;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author renazhou
 */

public class DatabaseSetup extends DatabaseQuery {
   
    PreparedStatement createGuestTable = null;
    PreparedStatement createPurchasesTable = null;
    PreparedStatement createStaffTable = null;
    PreparedStatement createRoomTable = null;
    PreparedStatement createReservationTable = null;
    ResultSet rs = null;
    
    public static void setupDatabase() {
        DatabaseSetup dbs = new DatabaseSetup();
        dbs.databaseSetup();
    }
    
    private void databaseSetup() {

        openConnection();
        
        try {

            // Determine if the RESERVATION table already exists or not
            DatabaseMetaData dbmd = conn.getMetaData();
            rs = dbmd.getTables(null, "APP", "RESERVATION", null);

            if (!rs.next()) {
                // If the RESERVATION table does not already exist we create it
                createReservationTable = conn.prepareStatement(
                "CREATE TABLE APP.RESERVATION ("
                        + "\"ID\" INT not null primary key "
                        + "GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), "
                        + "\"REFCODE\" VARCHAR(50),"
                        + "\"GUESTID\" VARCHAR(50),"
                        + "\"NUMPEOPLE\" VARCHAR(50),"        
                        + "\"ROOMNUMBER\" VARCHAR(50),"
                        + "\"CHECKIN\" VARCHAR(50),"
                        + "\"CHECKOUT\" VARCHAR(50),"
                        + "\"ADJUSTEDCHECK\" VARCHAR(50),"
                        + "\"BREAKFASTDATES\" VARCHAR(50),"
                        + "\"DEPOSIT\" INTEGER,"
                        + "\"COST\" INTEGER)"
                );
                createReservationTable.execute();
                /*
                createRoomTable = conn.prepareStatement(
                        "CREATE TABLE APP.ROOM ("
                        //+ "\"ID\" INT not null primary key "
                        + "GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), "
                        + "\"ROOMNUMBER\" VARCHAR(50),"  
                        + "\"ROOM_NAME\" VARCHAR(50),"
                        + "\"ROOM_DESC\" VARCHAR(50)),"
                        + "\"ROOM_COST\" INT)"
                );
                createRoomTable.execute();
                        */
                createStaffTable = conn.prepareStatement(
                        "CREATE TABLE APP.STAFF ("
                        + "\"ID\" INT not null primary key "
                        + "GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), "
                        + "\"LOGIN\" VARCHAR(100),"        
                        + "\"PASSWORD\" VARCHAR(100))"
                     
                );
                createStaffTable.execute();
                createPurchasesTable = conn.prepareStatement(
                        "CREATE TABLE APP.PURCHASES ("
                        + "\"ID\" INT not null primary key "
                        + "GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), "
                        + "\"PURCHASEQUANTITY\" VARCHAR(100),"
                        + "\"PURCHASEDESC\" VARCHAR(100),"
                        +"\"PURCHASECOST\" VARCHAR(100))"
                        
                );
                createPurchasesTable.execute();
                createGuestTable = conn.prepareStatement(
                        "CREATE TABLE APP.PURCHASES EMAIL("
                        + "\"ID\" INT not null primary key "
                        + "GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), "
                        + "\"FIRSTNAME\" VARCHAR(100),"
                        + "\"LASTNAME\" VARCHAR(100),"
                        + "\"PURCHASECOST\" VARCHAR(100),"
                        + "\"ADDRESS\" VARCHAR(100),"
                        + "\"PHONE\" VARCHAR(100))"
                
                );
                        
                
               StaffQuery sq = new StaffQuery(); 
               sq.insertStaff(new Staff("rena", "pw"));

            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        closeConnection();
    }
    
    
    
    
}
