/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.application.database;

import Model.Reservation;
import Model.Room;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author renazhou
 */
public class RoomViewQuery extends DatabaseQuery {
    
/*public List<Room> getRooms() {

        BufferedReader br = null;
        String line = "";
        String splitBy = ",";

        String[] rooms = null;
        List<Room> room = new ArrayList<Room>();
        try {

//            br = new BufferedReader(new FileReader("C:\\Users\\renazhou\\Documents\\Rooms(version 1).csv"));
            
            while ((line = br.readLine()) != null) {
                

                rooms = line.split(splitBy);

                Room temp = new Room(rooms[0], rooms[1], rooms[2], Integer.parseInt(rooms[3]));
                room.add(temp);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return room;
    }*/
    
        public List<Room> getRoomsFromFile() {
        List<Room> rooms = new ArrayList<Room>();
        try {
            Scanner scanner = new Scanner(new File("resources/rooms.csv"));

            while (scanner.hasNext()) {
                String r = scanner.nextLine();
                String[] terms = r.split(",");

                Room newRoom = new Room(
                        terms[0], // room num
                        terms[1], // name
                        terms[2], // description
                        Integer.parseInt(terms[3])); // cost per night

                rooms.add(newRoom);
            }

            // Close the file
            scanner.close();

        } catch (FileNotFoundException ex) {
        }
        return rooms;
    }

//Get days
//1. set a date or similar object to the start date (gregorian calendar)
//2. add it to list
//3. move the date to the next day and repeat until you reach departure date (plusDays)

    public List<String> getRoomNums() {
        List<String> roomNums = new ArrayList<>();
                for(Room rm : this.getRoomsFromFile()) {
        roomNums.add(rm.getNumber().getValue());
        }
        return roomNums;
        //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
    
}