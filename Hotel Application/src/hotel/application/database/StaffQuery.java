/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.application.database;

import Model.Staff;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author renazhou
 */
public class StaffQuery extends DatabaseQuery {

    public boolean login(String username, String password) {
        boolean access = false;

        openConnection();

        try {

            Statement stmt = conn.createStatement();
            String loginQuery = "SELECT * FROM APP.STAFF WHERE login = '" + username + "' AND password = '" + password + "' ";
            System.out.println(loginQuery);
            ResultSet rs = stmt.executeQuery(loginQuery);
            
            if(rs.next()) {
          
                access = true;
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        
        finally {
            closeConnection();
        }

        return access;
    }
    
    public void insertStaff(Staff s) {
        
    //private static Object staffLogin;
    PreparedStatement insertStaff = null; 
    PreparedStatement getStaff = null;
    
        ResultSet rs = null;

        openConnection();
        try {

            insertStaff = conn.prepareStatement("insert into app.STAFF (login, password) values (?, ?)", Statement.RETURN_GENERATED_KEYS);//
            insertStaff.setString(1, s.getUsername());
            insertStaff.setString(2, s.getPassword());

            insertStaff.executeUpdate();
            
            if(rs != null)
                rs.close();
            insertStaff.close();
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        closeConnection();
    }


}
