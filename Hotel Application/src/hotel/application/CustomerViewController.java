/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.application;

import Model.Guest;
import hotel.application.database.GuestQuery;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Tranny
 */
public class CustomerViewController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    @FXML
    private TableView customerTableView;
    
    @FXML
    private TableColumn guestIDColumn;
    
    @FXML
    private TableColumn firstNameColumn;
    
    @FXML
    private TableColumn lastNameColumn;
    
    @FXML 
    private TableColumn emailAddressColumn;
    
    @FXML
    private TableColumn contactNumberColumn;
    
    @FXML
    private TableColumn postalAddressColumn;
    

    @FXML
    private Pane createPaneButton;
    
    @FXML
    private Pane deletePaneButton;
    
    @FXML
    private Pane updatePaneButton;
    
    @FXML
    private Pane dashboardPaneButton;
    
    GuestQuery guestQuery = new GuestQuery();
    Guest guest;
    
    private ObservableList <Guest> guestData = FXCollections.observableArrayList();
    
    @FXML
    private void createHyperlinkHandle(Event event) throws IOException {
        //TODO: create - create customer gui
        Parent root = FXMLLoader.load(getClass().getResource("ReservationCreate.fxml"));
        Stage stage = new Stage();
        stage.setTitle("create new customer");
        stage.setScene(new Scene(root));
        stage.show();      
    }
    
    @FXML
    private void updateHyperlinkHandler(Event event){
        //GUI will look like Create reservation, when opened it will 
        //have all fields prefilled with selected data row
    }
    
    @FXML
    private void deleteHyperlinkHandle(Event event) throws IOException {
        //TODO: LOOK INTO THIS
        //LOGIC: actionlistener = click -> delete
    }

    @FXML
    private void dashboardHyperlinkHandle(Event event) throws IOException {        
        Parent root = FXMLLoader.load(getClass().getResource("Dashboard.fxml"));
        Scene scene = new Scene(root);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();      
        app_stage.setScene(scene);
        app_stage.show(); 
    }
            
          
            
          
}
