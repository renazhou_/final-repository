/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.application;

import Model.Reservation;
import hotel.application.database.ReservationQuery;
import hotel.application.database.RoomViewQuery;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Tranny
 */
public class ReservationCreateController implements Initializable {




    @FXML
    private TextField refCodeTextField;

    @FXML
    private ComboBox roomNumberComboBox;

    @FXML
    private TextField guestIDTextField;

    @FXML
    private TextField numGuestTextField;

    @FXML
    private ComboBox specialCheckInComboBox;
    
    @FXML
    private DatePicker arrivalDatePicker;

    @FXML
    private DatePicker departureDatePicker;

    @FXML
    private ComboBox freeBreakfastComboBox;

    @FXML
    private TextField depositPaidTextField;
    
    @FXML
    private TextField resCostTextField;

    @FXML
    private Button saveAndCompleteButton;

    @FXML
    private Button cancelButton;
    
    @FXML
    private Hyperlink dashboardHyperlink;
    
    ReservationQuery reservationQuery = new ReservationQuery();
    
    Reservation res;
    boolean isUpdate = false;
    
    private ObservableList<Reservation> resData = FXCollections.observableArrayList();
    
    @FXML
    private void dashboardHyperlinkHandle(ActionEvent event) throws IOException{
        Parent root = FXMLLoader.load(getClass().getResource("Dashboard.fxml"));
        Scene scene = new Scene(root);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(scene);
        app_stage.show();  
         
    }
    
    @FXML
    private void cancelButtonHandle(ActionEvent event){
        Stage stage = (Stage) cancelButton.getScene().getWindow();
        stage.close();
    }
     @FXML
    private void updateReservation() {

        String refCode = refCodeTextField.getText();
        String roomNum = (String) roomNumberComboBox.getValue();
        String guestID = guestIDTextField.getText();
        String numPeople = numGuestTextField.getText();
        LocalDate checkIn = arrivalDatePicker.getValue();
        java.util.Date arrivalDate = java.util.Date.from(checkIn.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());;

        LocalDate checkOut = departureDatePicker.getValue();
        java.util.Date checkOutDate = java.util.Date.from(checkOut.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());;

        String breakfast = (String) freeBreakfastComboBox.getValue();
        String adjustedCheck = (String) specialCheckInComboBox.getValue();
        
        Integer deposit = Integer.parseInt(depositPaidTextField.getText());
        Integer cost = Integer.parseInt(resCostTextField.getText());

        
        Reservation entry = new Reservation(refCode, guestID, numPeople, roomNum, new java.sql.Date(arrivalDate.getTime()), new java.sql.Date(checkOutDate.getTime()), breakfast, adjustedCheck, deposit, cost);

        resData.add(entry);
        reservationQuery.updateReservation(entry);
    }
    
    @FXML
    private void createReservation() {

        String refCode = refCodeTextField.getText();
        String roomNum = (String) roomNumberComboBox.getValue();
        String guestID = guestIDTextField.getText();
        String numPeople = numGuestTextField.getText();
        LocalDate checkIn = arrivalDatePicker.getValue();
        java.util.Date arrivalDate = java.util.Date.from(checkIn.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());;

        LocalDate checkOut = departureDatePicker.getValue();
        java.util.Date checkOutDate = java.util.Date.from(checkOut.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());;

        String breakfast = (String) freeBreakfastComboBox.getValue();
        String adjustedCheck = (String) specialCheckInComboBox.getValue();
        Integer deposit = Integer.parseInt(depositPaidTextField.getText());
        Integer cost = Integer.parseInt(resCostTextField.getText());

        
        Reservation entry = new Reservation(refCode, guestID, numPeople, roomNum, new java.sql.Date(arrivalDate.getTime()), new java.sql.Date(checkOutDate.getTime()), breakfast, adjustedCheck, deposit, cost);

        resData.add(entry);
        reservationQuery.insertReservation(entry);
    }
    /*
    @FXML
    private void populateTextFields(String ID) {

        String refCode = refCodeTextField.getText();
        String roomNum = (String) roomNumberComboBox.setValue();
        String guestID = guestIDTextField.setText();
        String numPeople = numGuestTextField.setText();
        //String checkIn = checkInTextField.getText();
        //String checkOut = checkOutTextField.getText();
        LocalDate checkIn = arrivalDatePicker.getValue();
        java.util.Date arrivalDate = java.util.Date.from(checkIn.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());;

        LocalDate checkOut = departureDatePicker.setValue();
        java.util.Date checkOutDate = java.util.Date.from(checkOut.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());;

        String breakfast = (String) freeBreakfastComboBox.setValue();
        String adjustedCheck = (String) specialCheckInComboBox.setValue();
        Double deposit = Double.parseDouble(depositPaidTextField.setText());
        Double cost = Double.parseDouble(resCostTextField.setText());

        
        Reservation entry = new Reservation(refCode, guestID, numPeople, roomNum, new java.sql.Date(arrivalDate.getTime()), new java.sql.Date(checkOutDate.getTime()), breakfast, adjustedCheck, deposit, cost);

        resData.add(entry);
        reservationQuery.insertReservation(entry);
    }
*/


    public void onSaveAndComplete(ActionEvent event) {
        if (isUpdate) {
            updateReservation();
        } else {
           createReservation();

        }
    }
    
    public void initData(Reservation r) {
        if (r != null) {
            // We're updating a row - prepopulate the fields
            refCodeTextField.setText(r.getRefCode());
            guestIDTextField.setText(r.getGuestID());
            numGuestTextField.setText(r.getNumPeople());
            
            refCodeTextField.setDisable(true);
            isUpdate = true;
        }
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        //TO DO
        ObservableList<String> roomNumOptions = FXCollections.observableArrayList();

        RoomViewQuery rq = new RoomViewQuery();
        roomNumOptions.addAll(rq.getRoomNums());
                
        roomNumberComboBox.setItems(roomNumOptions);
        /*
        ObservableList<String> breakfastDateOptions = FXCollections.observableArrayList();
        ReservationQuery resQ = new ReservationQuery();
        breakfastDateOptions.addAll(resQ.getDays());
        
        freeBreakfastComboBox.setItems(breakfastDateOptions);
       */
    }
                
    
   

}
