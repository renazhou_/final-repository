/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.application;

import Model.Reservation;
import Model.Room;
import hotel.application.database.DatabaseSetup;
import hotel.application.database.ReservationQuery;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.time.LocalDate;
import java.time.ZoneId;
import static java.time.temporal.TemporalQueries.localDate;
import java.util.GregorianCalendar;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author renazhou
 */
public class ReservationViewController implements Initializable {

    @FXML
    private TableView<Reservation> reservationTableView;

    @FXML
    TableColumn<Reservation, String> refCodeTableColumn;

    @FXML
    TableColumn<Reservation, String> guestIDTableColumn;

    @FXML
    TableColumn<Reservation, String> roomNumberTableColumn;

    @FXML
    TableColumn<Reservation, String> numPeopleTableColumn;

    @FXML
    TableColumn<Reservation, java.util.Date> arrivalDateTableColumn;

    @FXML
    TableColumn<Reservation, java.util.Date> departureDateTableColumn;

    @FXML
    TableColumn<Reservation, String> adjustedCheckTableColumn;

    @FXML
    TableColumn<Reservation, String> freeBreakfastTableColumn;

    @FXML
    TableColumn<Reservation, Double> depositPaidTableColumn;

    @FXML
    TableColumn<Reservation, Double> reservationCostTableColumn;

    @FXML
    private Pane dashboardPaneButton;

    @FXML
    private Pane createPaneButton;

    @FXML
    private Pane updatePaneButton;

    private Pane deletePaneButton;

    private ReservationQuery reservationQuery = new ReservationQuery();

    private ObservableList<Reservation> resData = FXCollections.observableArrayList();

    @FXML
    private void createHyperlinkHandle(Event event) throws IOException {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("ReservationCreate.fxml"));
        
        Stage stage = new Stage();
        stage.setTitle("Dashboard");
        stage.setScene(new Scene(loader.load()));
        
        ReservationCreateController controller =
                loader.<ReservationCreateController>getController();
        controller.initData(reservationTableView.getSelectionModel().getSelectedItem());
        
        stage.show();      

        Scene scene = new Scene(loader.load());
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(scene);
        app_stage.show();
    }

    @FXML
    private void updateHyperlinkHandler(Event event) {
        //GUI will look like Create reservation, when opened it will 
        //have all fields prefilled with selected data row
    }

    @FXML
    private void deleteHyperlinkHandle(Event event) throws IOException {
        //TODO: LOOK INTO THIS
        //LOGIC: actionlistener = click -> delete
        ReservationQuery rq = new ReservationQuery();
        rq.deleteReservation(reservationTableView.getSelectionModel().getSelectedItem());
    }

    @FXML
    private void dashboardHyperlinkHandle(Event event) throws IOException {        
        Parent root = FXMLLoader.load(getClass().getResource("Dashboard.fxml"));
        Scene scene = new Scene(root);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(scene);
        app_stage.show(); 
    }

    
    

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        refCodeTableColumn.setCellValueFactory(new PropertyValueFactory<Reservation, String>("refCode"));
        guestIDTableColumn.setCellValueFactory(new PropertyValueFactory<Reservation, String>("guestID"));
        numPeopleTableColumn.setCellValueFactory(new PropertyValueFactory<Reservation, String>("numPeople"));
        roomNumberTableColumn.setCellValueFactory(new PropertyValueFactory<Reservation, String>("roomNumber"));
        arrivalDateTableColumn.setCellValueFactory(new PropertyValueFactory<Reservation, java.util.Date>("checkIn"));
        departureDateTableColumn.setCellValueFactory(new PropertyValueFactory<Reservation, java.util.Date>("checkOut"));
        freeBreakfastTableColumn.setCellValueFactory(new PropertyValueFactory<Reservation, String>("freeBreakfast"));
        adjustedCheckTableColumn.setCellValueFactory(new PropertyValueFactory<Reservation, String>("adjustedDates"));
        depositPaidTableColumn.setCellValueFactory(new PropertyValueFactory<Reservation, Double>("deposit"));
        reservationCostTableColumn.setCellValueFactory(new PropertyValueFactory<Reservation, Double>("cost"));
        resData.addAll(reservationQuery.getReservation());
        reservationTableView.setItems(resData);

    
    }

}
