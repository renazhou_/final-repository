/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.application;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Tranny
 */
public class InvoiceController implements Initializable {

    @FXML
    private TableColumn purchaseDescriptionColumn;
    
    @FXML
    private TableColumn purchaseCostColumn;
    
    @FXML
    private TableColumn purchaseQuantityColumn;
    
    @FXML
    private TableColumn refCodeColumn;
    
    @FXML
    private TableColumn roomTypeColumn;
    
    @FXML
    private TableColumn numberOfDaysColumn;
    
    @FXML
    private TableColumn resCostColumn;
    
    @FXML
    private TextField invoiceIdTextField;
           
    @FXML
    private TextField guestIdTextField;   

    @FXML
    private TextField guestNameTextField;
    
    @FXML
    private TextArea addressTextArea;
    
    @FXML
    private DatePicker issueDatePicker;
    
    @FXML
    private DatePicker dueDatePicker;
    
    @FXML
    private TextField subTotalTextField;
    
    @FXML
    private TextField gstTextField;
    
    @FXML
    private TextField totalCostTextField;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
